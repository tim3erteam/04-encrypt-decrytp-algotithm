package main;

import caesar_cipher.CaesarCipher;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CaesarCipher caesarCipher = new CaesarCipher();
        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            dispalayMessage();
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("enter message to encrypt");
                    scanner.nextLine();

                    String mess = scanner.nextLine();
                    System.out.println("your message is " + mess);
                    String encrypt = caesarCipher.encryptByCaesarCipher(mess);
                    System.out.println("your encrypted message is " + encrypt);
                    String dencrypt = caesarCipher.dencryptByCaesarCipher(encrypt);
                    System.out.println("your dencrypted message is " + dencrypt);


                    break;
            }
        } while (option == 0);


    }

    private static void dispalayMessage() {
        System.out.println("enter ...  to ...\n"
            + "0->exit\n"
            + "1-> Ceasar Cipher");
    }
}
